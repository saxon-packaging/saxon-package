Name:     php-saxon
Version:  12.4.2
Release:  %autorelease
Summary:  PHP Saxon library
License:  MPL-2.0
URL:      https://www.saxonica.com
Source:   libsaxon-hec-linux.zip
#Patch:   0_fix_php8_XsltExecutable.patch

BuildRequires: make php-devel

%description
PHP module to use Saxon, the XSLT 3.0 and XQuery 3.0 processor.

%global sbdir %{_builddir}/%{name}-%{version}

%prep
%setup -q -c -T
# Unzip everything in a temp directory.
echo ${Source0}
unzip %{_sourcedir}/libsaxon-hec-linux.zip -d libsaxon
# Makes a copy of the C API sources.
mv ./libsaxon/*/Saxon.C.API/* %{sbdir}/
# Makes a copy of the library
cp -R ./libsaxon/*/libs/nix* %{sbdir}/nix_libs
cd %{sbdir}
#%patch0 -p1

%build
phpize
%configure \
    --enable-saxon
%make_build LDFLAGS="-L%{sbdir}/nix_libs"

%install
install -m 0755 -d ./etc/php.d
bash -c "echo "extension=saxon.so" > ./etc/php.d/20-saxon.ini"

mkdir -p %{sbdir}/%{_libdir}
%make_install INSTALL_ROOT=%{buildroot}

cp %{sbdir}/nix_libs/libsaxon-hec-*.so %{buildroot}/%{_libdir}/

%files
%{php_extdir}/saxon.so
%{_libdir}/libsaxon-hec-*.so

#%config(noreplace) %{php_inidir}/20-saxon.ini

%post
echo "extension=saxon.so" > %{php_inidir}/20-saxon.ini

%postun
# Removes the 20-saxon.ini configuration file on package removal.
if [ $1 -eq 0 ]; then
    rm -f %{php_inidir}/20-saxon.ini
fi

%changelog
%autochangelog
