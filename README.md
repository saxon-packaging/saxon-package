Saxon Packaging
---------------

This is the Saxon packaging repository. An unofficial packaging of Saxonica's Saxon
XSLT and XQuery processor.

The goal is to provide RPM spec files and patches to package Saxon products.

Licences
--------

All code, scripts and other artifacts present in this repository are licenced
under the _GPL-3.0-or-later_ licence, unless otherwise specified.

The Saxon product itself is made available by Saxonica under the terms
of the _MPL 2.0_ licence.

The patch files (files ending with '.patch') are under the _MPL 2.0_ licence
unless otherwise specified, as they make modifications of code from Saxonica.