Name:       saxon
Version:    12.5.0
Release:    %autorelease
Summary:    Saxon XSLT and XQuery processor
License:    MPL-2.0
URL:        https://www.saxonica.com
BuildArch:  noarch
# The source was pulled from upstream's vcs.
# Use the script 'generate-tarball.sh' to create the tarball.
Source0:    saxon-12.5.0.tar.gz
Patch0:     set_he_gradle.properties.patch
# TODO: use this gradle source instead of downloading it each time.
#Source1:    gradle-7.5-bin.zip

Requires:      java-headless
BuildRequires: java-17-openjdk-devel
# Used to apply patches.
BuildRequires: git

%description
A highly conformant implementation of the current W3C Recommendations:
XSLT 3.0, XQuery 3.1, XPath 3.1, and XSD 1.1.
This is a package for the Java product (SaxonJ) with the Saxon-HE edition,
which includes support for the published W3C recommendations at the minimum
conformance level.

%global sbdir %{_builddir}/%{name}-%{version}
%global xmlresolver_version 5.2.2

%prep
ls -lah %{_sourcedir}
mkdir %{name}-%{version}
%setup -q -c

mv %{_sourcedir}/*.patch %{sbdir}
cd %{sbdir}
find . -type f -name '*.jar'
# Removes all the prebuilt Jars except the Gradle one.
find . -type f -name '*.jar' ! -name "gradle-wrapper.jar" -delete
ls -lah

# Sets 'HE' as the product we build.
# This is the open source edition with the MPL-2.0
git apply set_he_gradle.properties.patch
cd ..

%build
ls -lah
cd %{sbdir}
cat ./gradle.properties

# Explicitly sets the encoding during the build.
export GRADLE_OPTS="-Dfile.encoding=utf-8"
# Here, Gradle will download dependencies.
./gradlew hej_preprocess

# Use to check that the processor's JVM uses the correct file encoding.
# If you find '???' in char assignments, there is probably an issue.
# head -n 30 build/src/hej/net/sf/saxon/expr/parser/Tokenizer.java

# This file is required, but I could not find a gradle task to copy it properly.
mkdir -p  ./build/resources/main/net/sf/saxon/data/sef/
cp ./src/archive/csharp/build/cs/resources/sef/function-library.sef.xml ./build/resources/main/net/sf/saxon/data/sef/

# Builds the classes and prepares the Jar.
./gradlew hejJar
# Releases everything into the build/release directory.
./gradlew hejRelease


%install
%global saxon_install_dir /%{buildroot}%{_javadir}/saxon
install -m 0755 -d %{saxon_install_dir}
# I was not able to run Saxon when it is installed next to its dependencies.
# I did not try more, but it may be possible. For now, we just create this 'lib'
# directory like Saxonica does in their release zip.
install -m 0755 -d %{saxon_install_dir}/lib

ls -R %{sbdir}/build/releases

# Copies the released artifact.
cp %{sbdir}/build/releases/hej/saxon-he-12*.jar %{saxon_install_dir}/saxon-he.jar
# Copies the dependencies.
cp %{sbdir}/build/releases/hej/lib/jline*.jar %{saxon_install_dir}/lib/jline.jar
cp %{sbdir}/build/releases/hej/lib/xmlresolver-%{xmlresolver_version}-data.jar %{saxon_install_dir}/lib/xmlresolver-data.jar
cp %{sbdir}/build/releases/hej/lib/xmlresolver-%{xmlresolver_version}.jar %{saxon_install_dir}/lib/xmlresolver.jar

%files
%{_javadir}/saxon/saxon-he.jar
%{_javadir}/saxon/lib/jline.jar
%{_javadir}/saxon/lib/xmlresolver-data.jar
%{_javadir}/saxon/lib/xmlresolver.jar

%post

%postun

%changelog
%autochangelog
