#!/bin/sh


git clone https://saxonica.plan.io/saxonica/saxonmirrorhe.he.git saxon_repo

cd saxon_repo
git checkout he_mirror_saxon_12_5
cd ..

# This just means: create an archive named 'saxon-12.5.0tar.gz' with
# all the content in the directory 'saxon_repo'.
# It may have looked like 'tar -cJvf saxon-12.5.0.tar.gz saxon_repo/**',
# but it would have been too simple to allow people to use intuitive
# commands. Or I misread the documentation.
tar -cJvf saxon-12.5.0.tar.gz -C saxon_repo --strip-components=1 ./

